#!/bin/bash
source ./func

${display} "請輸入要更動的產品:\n1:NN\n2:LZ\n3:HF"
${red} "輸入:" product
case ${product} in
1)
  ${cng} 's/ADD\ .*\/vhosts \/usr\/local\/nginx\/conf\/vhosts/ADD\ nn\/vhosts \/usr\/local\/nginx\/conf\/vhosts/g' ${patd}/Dockerfile
  ${display} "目前已存在的docker images版本:\n\n"; ${dkimg}|awk '{print $1":"$2}' |grep '^nginx'
  source ./work
  ;;
2)
  ${cng} 's/ADD\ .*\/vhosts \/usr\/local\/nginx\/conf\/vhosts/ADD\ lz\/vhosts \/usr\/local\/nginx\/conf\/vhosts/g' ${patd}/Dockerfile
  ${display} "目前已存在的docker images版本:\n\n"; ${dkimg}|awk '{print $1":"$2}' |grep '^lznginx'
  source ./work
  ;;
esac
