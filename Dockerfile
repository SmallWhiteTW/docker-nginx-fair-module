FROM centos:7
MAINTAINER white

RUN yum -y install unzip openssl* ncurses-devel gcc gcc++ pcre pcre-devel zlib zlib-devel zlib1g zlib1g-dev patch iproute iproute-doc vim telnet

RUN cd /tmp

ADD nginx-1.14.0.tar.gz /tmp
ADD nginx_upstream_check_module-master.zip /tmp
ADD nginx-upstream-fair-master.zip /tmp

RUN cd /tmp;unzip nginx_upstream_check_module-master.zip
RUN cd /tmp;unzip nginx-upstream-fair-master.zip

RUN cd /tmp/nginx-upstream-fair-master;patch -p1 < ../nginx_upstream_check_module-master/upstream_fair.patch
RUN cd /tmp/nginx-1.14.0/;patch -p1 < ../nginx_upstream_check_module-master/check_1.12.1+.patch

RUN cd /tmp/nginx-1.14.0/;./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module --with-http_gzip_static_module --with-pcre --add-module=/tmp/nginx_upstream_check_module-master --add-module=/tmp/nginx-upstream-fair-master;make && make install

RUN mkdir -p /usr/local/nginx/conf/vhosts
RUN mkdir -p /var/log/nginx

ADD cert /usr/local/nginx/conf/cert
ADD nginx.conf /usr/local/nginx/conf
ADD lz/vhosts /usr/local/nginx/conf/vhosts

RUN cd /bin;ln -ins /usr/local/nginx/sbin/nginx nginx

ADD nginx.service /usr/lib/systemd/system/

ADD run.sh /

CMD ["/run.sh"]
